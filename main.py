def exit_maze(maze: list, start: list) -> list:
    matrix = [[1 if x == '.' else 0 for x in y] for y in maze]
    height = len(matrix) - 1
    points = [start]
    while points:
        x, y = points.pop()
        matrix[x][y] = 0
        width = len(matrix[x]) - 1
        ways = ((x-1, y),
                (x+1, y),
                (x, y-1),
                (x, y+1))
        steps = []
        for a, b in ways:
            try:
                if matrix[a][b]:
                    steps.append([a, b])
            except IndexError:
                continue

        if not steps and \
                (x in (0, height) or y in (0, width)):
            return [x, y]

        points.extend(steps)

    return []



if __name__ == '__main__':
    lst = [
        '##########',
        '.........#',
        '######.###',
        '#......###',
        '#.####.###',
        '#........#',
        '##.#######',
        '##.##.####',
        '##......##',
        '#######.##'
    ]
    enter = [1, 0]

    assert exit_maze(lst, enter) == [9, 7]
