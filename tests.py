from unittest import TestCase
from main import exit_maze


class MazeTestCase(TestCase):

    def test_no_exit_maze(self):
        maze = ['#.#',
                '#.#',
                '###']
        start = [0, 1]
        expected_value = []
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_top_bottom_maze(self):
        maze = ['#.#',
                '#.#',
                '#.#']
        start = [0, 1]
        expected_value = [2, 1]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_top_right_maze(self):
        maze = ['#.#',
                '..#',
                '###']
        start = [0, 1]
        expected_value = [1, 0]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_top_left_maze(self):
        maze = ['#.#',
                '#..',
                '###']
        start = [0, 1]
        expected_value = [1, 2]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_bottom_top_maze(self):
        maze = ['#.#',
                '#.#',
                '#.#']
        start = [2, 1]
        expected_value = [0, 1]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_bottom_right_maze(self):
        maze = ['###',
                '..#',
                '#.#']
        start = [2, 1]
        expected_value = [1, 0]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_bottom_left_maze(self):
        maze = ['###',
                '#..',
                '#.#']
        start = [2, 1]
        expected_value = [1, 2]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_right_left_maze(self):
        maze = ['###',
                '...',
                '###']
        start = [1, 0]
        expected_value = [1, 2]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_right_top_maze(self):
        maze = ['#.#',
                '..#',
                '###']
        start = [1, 0]
        expected_value = [0, 1]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_right_bottom_maze(self):
        maze = ['###',
                '..#',
                '#.#']
        start = [1, 0]
        expected_value = [2, 1]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_left_right_maze(self):
        maze = ['###',
                '...',
                '###']
        start = [1, 2]
        expected_value = [1, 0]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_left_top_maze(self):
        maze = ['#.#',
                '#..',
                '###']
        start = [1, 2]
        expected_value = [0, 1]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_left_bottom_maze(self):
        maze = ['###',
                '#..',
                '#.#']
        start = [1, 2]
        expected_value = [2, 1]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)


    def test_top_top_maze(self):
        maze = ['#.#.#',
                '#...#',
                '#####']
        start = [0, 1]
        expected_value = [0, 3]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_bottom_bottom_maze(self):
        maze = ['#####',
                '#...#',
                '#.#.#']
        start = [2, 1]
        expected_value = [2, 3]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_right_right_maze(self):
        maze = ['###',
                '..#',
                '#.#',
                '..#',
                '###']
        start = [1, 0]
        expected_value = [3, 0]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_left_left_maze(self):
        maze = ['###',
                '#..',
                '#.#',
                '#..',
                '###']
        start = [1, 2]
        expected_value = [3, 2]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)

    def test_maze(self):

        maze = ['####################',
                '..#............#...#',
                '#.#.#####..#.#.#.#.#',
                '#.#.#.#.#..#.#.#.#.#',
                '#.#.#.#.#..#.#.#.#.#',
                '#.#.#.#.#..#.#.#.#.#',
                '#.#.#.#.#..#.#.#.#.#',
                '#.#.#.#.#..#.#.#.#.#',
                '#.#.#.#.#..#.#.#.#.#',
                '#.#.#.#.#..#.#.#.#.#',
                '#.#.#.#.#..#.#.#.#.#',
                '#.#.#.#.#..#.#.#.#.#',
                '#.#.#.#.#..#####.#.#',
                '#...#............#..',
                '####################']
        start = [1, 0]
        expected_value = [13, 19]
        value = exit_maze(maze, start)
        self.assertEqual(expected_value, value)
